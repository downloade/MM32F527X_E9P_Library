/*********************************************************************************************************************
* MM32F527X-E9P Opensource Library 即（MM32F527X-E9P 开源库）是一个基于官方 SDK 接口的第三方开源库
* Copyright (c) 2022 SEEKFREE 逐飞科技
* 
* 本文件是 MM32F527X-E9P 开源库的一部分
* 
* MM32F527X-E9P 开源库 是免费软件
* 您可以根据自由软件基金会发布的 GPL（GNU General Public License，即 GNU通用公共许可证）的条款
* 即 GPL 的第3版（即 GPL3.0）或（您选择的）任何后来的版本，重新发布和/或修改它
* 
* 本开源库的发布是希望它能发挥作用，但并未对其作任何的保证
* 甚至没有隐含的适销性或适合特定用途的保证
* 更多细节请参见 GPL
* 
* 您应该在收到本开源库的同时收到一份 GPL 的副本
* 如果没有，请参阅<https://www.gnu.org/licenses/>
* 
* 额外注明：
* 本开源库使用 GPL3.0 开源许可证协议 以上许可申明为译文版本
* 许可申明英文版在 libraries/doc 文件夹下的 GPL3_permission_statement.txt 文件中
* 许可证副本在 libraries 文件夹下 即该文件夹下的 LICENSE 文件
* 欢迎各位使用并传播本程序 但修改内容时必须保留逐飞科技的版权声明（即本声明）
* 
* 文件名称          zf_device_ov7725
* 公司名称          成都逐飞科技有限公司
* 版本信息          查看 libraries/doc 文件夹内 version 文件 版本说明
* 开发环境          MDK 5.37
* 适用平台          MM32F527X_E9P
* 店铺链接          https://seekfree.taobao.com/
* 
* 修改记录
* 日期              作者                备注
* 2022-08-10        Teternal            first version
********************************************************************************************************************/
/*********************************************************************************************************************
* 接线定义：
*                   ------------------------------------
*                   模块管脚            单片机管脚
*                   TXD/SCL             查看 zf_device_ov7725.h 中 OV7725_COF_UART_TX 或 OV7725_COF_IIC_SCL 宏定义
*                   RXD/SDA             查看 zf_device_ov7725.h 中 OV7725_COF_UART_RX 或 OV7725_COF_IIC_SDA 宏定义
*                   PCLK                查看 zf_device_ov7725.h 中 OV7725_PCLK_PIN 宏定义
*                   VSY                 查看 zf_device_ov7725.h 中 OV7725_VSYNC_PIN 宏定义
*                   D0-D7               查看 zf_device_ov7725.h 中 OV7725_DATA_PIN 宏定义 从该定义开始的连续八个引脚
*                   VCC                 3.3V电源
*                   GND                 电源地
*                   其余引脚悬空
*                   ------------------------------------
********************************************************************************************************************/

#include "zf_common_debug.h"
#include "zf_common_fifo.h"

#include "zf_driver_delay.h"
#include "zf_driver_dma.h"
#include "zf_driver_exti.h"
#include "zf_driver_gpio.h"
#include "zf_driver_uart.h"
#include "zf_driver_soft_iic.h"
#include "zf_driver_uart.h"
#include "zf_driver_timer.h"

#include "zf_device_config.h"
#include "zf_device_camera.h"
#include "zf_device_type.h"

#include "zf_device_ov7725.h"

vuint8 ov7725_finish_flag = 0;
uint8 ov7725_image_binary [OV7725_H][OV7725_W / 8];

static fifo_struct          *camera_receiver_fifo;

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     配置摄像头内部配置信息
// 参数说明     buff            发送配置信息的地址
// 返回参数     uint8           1-失败 0-成功
// 使用示例     if(ov7725_set_config(ov7725_set_confing_buffer)){}
// 备注信息     内部调用
//-------------------------------------------------------------------------------------------------------------------
static uint8 ov7725_set_config (const uint16 buff[OV7725_CONFIG_FINISH][2])
{
    uint8 return_state = 1;
    uint8  uart_buffer[4];
    uint16 temp = 0;
    uint16 timeout_count = 0;
    uint32 loop_count = 0;
    uint32 uart_buffer_index = 0;

    for(loop_count = OV7725_ROW; OV7725_SET_DATA > loop_count; loop_count --)
    {
        uart_buffer[0] = 0xA5;
        uart_buffer[1] = buff[loop_count][0];
        temp           = buff[loop_count][1];
        uart_buffer[2] = temp >> 8;
        uart_buffer[3] = (uint8)temp;

        uart_write_buffer(OV7725_COF_UART, uart_buffer, 4);
        system_delay_ms(10);
    }

    do
    {
        if(3 <= fifo_used(camera_receiver_fifo))
        {
            uart_buffer_index = 3;
            fifo_read_buffer(camera_receiver_fifo, uart_buffer, &uart_buffer_index, FIFO_READ_AND_CLEAN);
            if((0xff == uart_buffer[1]) || (0xff == uart_buffer[2]))
            {
                return_state = 0;
                break;
            }
        }
        system_delay_ms(1);
    }while(OV7725_INIT_TIMEOUT > timeout_count ++);

    // 以上部分对摄像头配置的数据全部都会保存在摄像头上51单片机的eeprom中
    // 利用set_exposure_time函数单独配置的曝光数据不存储在eeprom中
    return return_state;
}

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     获取摄像头内部配置信息
// 参数说明     buff            接收配置信息的地址
// 返回参数     uint8           1-失败 0-成功
// 使用示例     if(ov7725_get_config(ov7725_get_confing_buffer)){}
// 备注信息     内部调用
//-------------------------------------------------------------------------------------------------------------------
static uint8 ov7725_get_config (uint16 buff[OV7725_CONFIG_FINISH - 1][2])
{
    int8   loop_count = 0;
    uint8  return_state = 0;
    uint8  uart_buffer[4];
    uint16 temp = 0;
    uint16 timeout_count = 0;
    uint32 uart_buffer_index = 0;

    for(loop_count = OV7725_ROW - 1; 1 <= loop_count; loop_count --)
    {
        uart_buffer[0] = 0xA5;
        uart_buffer[1] = OV7725_GET_STATUS;
        temp           = buff[loop_count][0];
        uart_buffer[2] = temp >> 8;
        uart_buffer[3] = (uint8)temp;

        uart_write_buffer(OV7725_COF_UART, uart_buffer, 4);

        system_delay_ms(10);

        timeout_count = 0;    
        do
        {
            if(3 <= fifo_used(camera_receiver_fifo))
            {
                uart_buffer_index = 3;
                fifo_read_buffer(camera_receiver_fifo, uart_buffer, &uart_buffer_index, FIFO_READ_AND_CLEAN);
                buff[loop_count][1] = uart_buffer[1] << 8 | uart_buffer[2];
                break;
            }
            system_delay_ms(1);
        }while(OV7725_INIT_TIMEOUT > timeout_count ++);
        if(OV7725_INIT_TIMEOUT < timeout_count)                                 // 超时
        {
            return_state = 1;
            break;
        }
    }
    return return_state;
}

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     OV7725 UART 获取摄像头 ID
// 参数说明     void
// 返回参数     uint8           0-成功 x-失败
// 使用示例     ov7725_uart_check_id();
// 备注信息     内部调用
//-------------------------------------------------------------------------------------------------------------------
static uint8 ov7725_uart_check_id (void)
{
    uint8   uart_buffer[4];
    uint8   return_state        = 1;
    uint16  timeout_count       = 0;
    uint32  uart_buffer_index   = 0;
    
    if(fifo_used(camera_receiver_fifo))
    {
        fifo_clear(camera_receiver_fifo);
    }
    
    uart_write_byte(OV7725_COF_UART, 0xFF);
    uart_buffer[0]  = 0xA5;
    uart_buffer[1]  = OV7725_GET_WHO_AM_I;
    uart_write_buffer(OV7725_COF_UART, uart_buffer, 4);

    timeout_count = 0;    
    do
    {
        if(3 <= fifo_used(camera_receiver_fifo))
        {
            uart_buffer_index = 3;
            fifo_read_buffer(camera_receiver_fifo, uart_buffer, &uart_buffer_index, FIFO_READ_AND_CLEAN);
            return_state = !(0x00 == uart_buffer[2]);
            break;
        }
        system_delay_ms(1);
    }while(OV7725_INIT_TIMEOUT > timeout_count ++);
        
    return return_state;
}

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     OV7725 通信串口回调函数
// 参数说明     void
// 返回参数     void
// 使用示例     ov7725_uart_handler();
// 备注信息     通过 zf_device_type.c 的接口调用 用户在使用默认设置时不需要关心
//-------------------------------------------------------------------------------------------------------------------
static void ov7725_uart_handler (void)
{
    uint8 data = 0;
    uart_query_byte(OV7725_COF_UART, &data);
    if(NULL != camera_receiver_fifo)
    {
        if(0xA5 == data)
        {
            fifo_clear(camera_receiver_fifo);
        }
        fifo_write_element(camera_receiver_fifo, data);
    }
}

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     OV7725 场中断回调函数
// 参数说明     void
// 返回参数     void
// 使用示例     ov7725_vsync_handler();
// 备注信息     通过 zf_device_type.c 的接口调用 用户在使用默认设置时不需要关心
//-------------------------------------------------------------------------------------------------------------------
static void ov7725_vsync_handler (void)
{
    dma_disable(OV7725_DMA_CH);
    dma_set_count(OV7725_DMA_CH, OV7725_IMAGE_SIZE);                            // 设置当前DMA传输的剩余数量 向下递减 该寄存器只能在DMA不工作时更改。
    dma_enable(OV7725_DMA_CH);
    dma_disable(OV7725_DMA_CH);
    dma_set_count(OV7725_DMA_CH, OV7725_IMAGE_SIZE);                            // 设置当前DMA传输的剩余数量 向下递减 该寄存器只能在DMA不工作时更改。
    dma_enable(OV7725_DMA_CH);
}

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     OV7725 DMA 完成中断回调函数
// 参数说明     void
// 返回参数     void
// 使用示例     ov7725_dma_handler();
// 备注信息     通过 zf_device_type.c 的接口调用 用户在使用默认设置时不需要关心
//-------------------------------------------------------------------------------------------------------------------
static void ov7725_dma_handler (void)
{
    dma_disable(OV7725_DMA_CH);
    ov7725_finish_flag = 1;
}

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     获取摄像头固件版本
// 参数说明     void
// 返回参数     void
// 使用示例     ov7725_get_version();                           // 调用该函数前请先初始化串口
// 备注信息     内部调用
//-------------------------------------------------------------------------------------------------------------------
uint16 ov7725_get_version (void)
{
    uint8   uart_buffer[4];
    uint16  temp_value          = 0;
    uint16  timeout_count       = 0;
    uint16  return_value        = 0;
    uint32  uart_buffer_index   = 0;
    
    uart_buffer[0]  = 0xA5;
    uart_buffer[1]  = OV7725_GET_STATUS;
    temp_value      = OV7725_GET_VERSION;
    uart_buffer[2]  = temp_value >> 8;
    uart_buffer[3]  = (uint8)temp_value;

    uart_write_buffer(OV7725_COF_UART, uart_buffer, 4);

    do
    {
        if(3 <= fifo_used(camera_receiver_fifo))
        {
            uart_buffer_index = 3;
            fifo_read_buffer(camera_receiver_fifo, uart_buffer, &uart_buffer_index, FIFO_READ_AND_CLEAN);
            return_value = uart_buffer[1] << 8 | uart_buffer[2];
            break;
        }
        system_delay_ms(1);
    }while(OV7725_INIT_TIMEOUT > timeout_count ++);
    
    return return_value;
}

//-------------------------------------------------------------------------------------------------------------------
// 函数简介     OV7725 摄像头初始化
// 参数说明     NULL
// 返回参数     void
// 使用示例     ov7725_init();
// 备注信息     
//-------------------------------------------------------------------------------------------------------------------
uint8 ov7725_init (void)
{
    uint8 loop_count        = 0;
    uint8 return_state      = 0;
    uint8 *receiver_buffer  = NULL;
    soft_iic_info_struct ov7725_iic_struct;

    // 需要配置到摄像头的数据 不允许在这修改参数
    const uint16 ov7725_set_confing_buffer [OV7725_CONFIG_FINISH][2]=
    {
        {OV7725_INIT,           0},                                             // 初始化命令

        {OV7725_RESERVE,        0},                                             // 保留
        {OV7725_CONTRAST,       OV7725_CONTRAST_DEF},                           // 阈值设置
        {OV7725_FPS,            OV7725_FPS_DEF},                                // 帧率
        {OV7725_COL,            OV7725_W},                                      // 图像宽度
        {OV7725_ROW,            OV7725_H}                                       // 图像高度
    };

    // 从摄像头内部获取到的配置数据 不允许在这修改参数
    uint16 ov7725_get_confing_buffer [OV7725_CONFIG_FINISH - 1][2]=
    {
        {OV7725_RESERVE,        0},                                             // 保留
        {OV7725_CONTRAST,       0},                                             // 阈值设置
        {OV7725_FPS,            0},                                             // 帧率
        {OV7725_COL,            0},                                             // 图像宽度
        {OV7725_ROW,            0}                                              // 图像高度
    };

    do
    {
        soft_iic_init(&ov7725_iic_struct,                                       // 配置 SCCB
            0,                                                                  // 地址暂设为 0
            OV7725_COF_IIC_DELAY,                                               // 时钟延迟
            OV7725_COF_IIC_SCL,                                                 // SCL 引脚
            OV7725_COF_IIC_SDA);                                                // SDA 引脚
        if(!ov7725_sccb_check_id(&ov7725_iic_struct))
        {
            set_camera_type(CAMERA_BIN_IIC, ov7725_vsync_handler, ov7725_dma_handler, NULL);
            return_state = ov7725_sccb_set_config(ov7725_set_confing_buffer);
        }
        else
        {
            set_camera_type(CAMERA_BIN_UART, ov7725_vsync_handler, ov7725_dma_handler, ov7725_uart_handler);
            receiver_buffer = (uint8 *)malloc(OV7725_COF_BUFFER_SIZE);          // 申请缓冲区
            if(NULL == receiver_buffer)                                         // 堆空间不足
            {
                zf_log(0, "FIFO buffer malloc error.");                         // 无法获取数据缓冲区提示
                return_state = 2;                                               // 异常码
                break;
            }
            camera_receiver_fifo = (fifo_struct *)malloc(sizeof(fifo_struct));  // 申请 FIFO 对象
            if(NULL == camera_receiver_fifo)                                    // 堆空间不足
            {
                zf_log(0, "FIFO malloc error.");                                // 无法获取 FIFO 对象提示
                return_state = 2;                                               // 异常码
                break;
            }
            fifo_init(camera_receiver_fifo, FIFO_DATA_8BIT, receiver_buffer, OV7725_COF_BUFFER_SIZE);
            
            uart_init(OV7725_COF_UART, OV7725_COF_BAUR, OV7725_COF_UART_RX, OV7725_COF_UART_TX);
            uart_rx_interrupt(OV7725_COF_UART, 1);
            
            if(ov7725_uart_check_id())
            {
//                zf_log(0, "OV7725 ID error.");
                uart_rx_interrupt(OV7725_COF_UART, 0);
                return_state = 3;
                break;
            }
            else
            {
                // 设置所有参数
                if(ov7725_set_config(ov7725_set_confing_buffer))
                {
                    // 如果程序在输出了断言信息 并且提示出错位置在这里
                    // 那么就是串口通信出错并超时退出了
                    // 检查一下接线有没有问题 如果没问题可能就是坏了
                    zf_log(0, "OV7725 set confing error.");
                    uart_rx_interrupt(OV7725_COF_UART, 0);
                    return_state = 1;
                    break;
                }
                // 获取所有参数
                if(ov7725_get_config(ov7725_get_confing_buffer))
                {
                    // 如果程序在输出了断言信息 并且提示出错位置在这里
                    // 那么就是串口通信出错并超时退出了
                    // 检查一下接线有没有问题 如果没问题可能就是坏了
                    zf_log(0, "OV7725 get confing error.");
                    uart_rx_interrupt(OV7725_COF_UART, 0);
                    return_state = 1;
                    break;
                }
                
                for(loop_count = 0; OV7725_CONFIG_FINISH - 1 > loop_count; loop_count ++)
                {
                    if( ov7725_set_confing_buffer[loop_count + 1][0] != ov7725_get_confing_buffer[loop_count][0] ||
                        ov7725_set_confing_buffer[loop_count + 1][1] != ov7725_get_confing_buffer[loop_count][1])
                    {
                        break;
                    }
                }
                return_state = (OV7725_CONFIG_FINISH - 1 == loop_count) ? 0 : 1;
            }
        }
        if(!return_state)
        {
            gpio_init(OV7725_VSYNC_PIN, GPI, GPIO_LOW, GPI_FLOATING_IN);
            for(loop_count = 0; 8 > loop_count; loop_count ++)
            {
                gpio_init((gpio_pin_enum)(OV7725_DATA_PIN + loop_count), GPI, GPIO_LOW, GPI_FLOATING_IN);
            }
        
            while((gpio_get_level(OV7725_VSYNC_PIN)));
            camera_init(OV7725_IMAGE_SIZE, OV7725_DATA_ADD, (uint32)&ov7725_image_binary[0][0]);
            break;
        }
        set_camera_type(NO_CAMERE, NULL, NULL, NULL);
    }while(0);

    free(receiver_buffer);
    free(camera_receiver_fifo);
    camera_receiver_fifo = NULL;
    return return_state;
}
