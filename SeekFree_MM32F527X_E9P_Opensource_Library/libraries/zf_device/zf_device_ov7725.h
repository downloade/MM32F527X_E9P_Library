/*********************************************************************************************************************
* MM32F527X-E9P Opensource Library 即（MM32F527X-E9P 开源库）是一个基于官方 SDK 接口的第三方开源库
* Copyright (c) 2022 SEEKFREE 逐飞科技
* 
* 本文件是 MM32F527X-E9P 开源库的一部分
* 
* MM32F527X-E9P 开源库 是免费软件
* 您可以根据自由软件基金会发布的 GPL（GNU General Public License，即 GNU通用公共许可证）的条款
* 即 GPL 的第3版（即 GPL3.0）或（您选择的）任何后来的版本，重新发布和/或修改它
* 
* 本开源库的发布是希望它能发挥作用，但并未对其作任何的保证
* 甚至没有隐含的适销性或适合特定用途的保证
* 更多细节请参见 GPL
* 
* 您应该在收到本开源库的同时收到一份 GPL 的副本
* 如果没有，请参阅<https://www.gnu.org/licenses/>
* 
* 额外注明：
* 本开源库使用 GPL3.0 开源许可证协议 以上许可申明为译文版本
* 许可申明英文版在 libraries/doc 文件夹下的 GPL3_permission_statement.txt 文件中
* 许可证副本在 libraries 文件夹下 即该文件夹下的 LICENSE 文件
* 欢迎各位使用并传播本程序 但修改内容时必须保留逐飞科技的版权声明（即本声明）
* 
* 文件名称          zf_device_ov7725
* 公司名称          成都逐飞科技有限公司
* 版本信息          查看 libraries/doc 文件夹内 version 文件 版本说明
* 开发环境          MDK 5.37
* 适用平台          MM32F527X_E9P
* 店铺链接          https://seekfree.taobao.com/
* 
* 修改记录
* 日期              作者                备注
* 2022-08-10        Teternal            first version
********************************************************************************************************************/
/*********************************************************************************************************************
* 接线定义：
*                   ------------------------------------
*                   模块管脚            单片机管脚
*                   TXD/SCL             查看 zf_device_ov7725.h 中 OV7725_COF_UART_TX 或 OV7725_COF_IIC_SCL 宏定义
*                   RXD/SDA             查看 zf_device_ov7725.h 中 OV7725_COF_UART_RX 或 OV7725_COF_IIC_SDA 宏定义
*                   PCLK                查看 zf_device_ov7725.h 中 OV7725_PCLK_PIN 宏定义
*                   VSY                 查看 zf_device_ov7725.h 中 OV7725_VSYNC_PIN 宏定义
*                   D0-D7               查看 zf_device_ov7725.h 中 OV7725_DATA_PIN 宏定义 从该定义开始的连续八个引脚
*                   VCC                 3.3V电源
*                   GND                 电源地
*                   其余引脚悬空
*                   ------------------------------------
********************************************************************************************************************/

#ifndef _zf_device_ov7725_h_
#define _zf_device_ov7725_h_

#include "zf_common_typedef.h"

//--------------------------------------------------------------------------------------------------
// 引脚配置
//--------------------------------------------------------------------------------------------------
#define OV7725_COF_UART         ( UART_5       )                                // 小钻风配置串口
#define OV7725_COF_BAUR         ( 9600         )                                // 小钻风配置串口波特率
#define OV7725_COF_UART_TX      ( UART5_RX_D2  )                                // 小钻风 UART-TX 引脚 要接在单片机 RX 上
#define OV7725_COF_UART_RX      ( UART5_TX_C12 )                                // 小钻风 UART-RX 引脚 要接在单片机 TX 上

#define OV7725_COF_IIC_DELAY    ( 150 )                                         // 小钻风 IIC 延时
#define OV7725_COF_IIC_SCL      ( D2  )                                         // 小钻风 IIC-SCL 引脚
#define OV7725_COF_IIC_SDA      ( C12 )                                         // 小钻风 IIC-SDA 引脚

#define OV7725_DMA_CH           ( DMA1_CHANNEL4 )                               // TIM触发DMA通道禁止随意修改
#define OV7725_DMA_IRQN         ( DMA1_CH4_IRQn )                               // DMA中断通道

#define OV7725_PCLK_PIN         ( TIM1_ETR_E7 )                                 // GPIO触发TIM引脚禁止随意修改

#define OV7725_VSYNC_PIN        ( E8            )                               // 场中断引脚
#define OV7725_VSYNC_IRQN       ( EXTI9_5_IRQn  )                               // 中断号

#define OV7725_DATA_PIN         ( G0 )                                          // 数据引脚 这里是 只能是 GPIOx0 或者 GPIOx8 开始 连续八个引脚例如 F0-F7
#define OV7725_DATA_ADD         (gpio_idr_addr(OV7725_DATA_PIN))

#define OV7725_INIT_TIMEOUT     ( 127 )                                         // 默认的摄像头初始化超时时间 毫秒为单位

//--------------------------------------------------------------------------------------------------
// 摄像头默认参数配置 在此修改摄像头配置
//--------------------------------------------------------------------------------------------------
#define OV7725_W                ( 160 )                                         // 图像宽度 80/160/240/320
#define OV7725_H                ( 120 )                                         // 图像高度 60/120/180/240
#define OV7725_IMAGE_SIZE       ( OV7725_W * OV7725_H / 8 )                     // 整体图像大小 OV7725_IMAGE_SIZE 不能超过 65535
#define OV7725_COF_BUFFER_SIZE  ( 64  )                                         // 配置串口缓冲区大小

#define OV7725_CONTRAST_DEF     ( 0x30 )                                        // 阈值设置 摄像头二值化阈值 过大和过小的数值会被摄像头自动修正
#define OV7725_FPS_DEF          ( OV7725_FPS_50 )                               // 帧率设置 50/75/100/112/150 最高 150 帧 但最小分辨率才能达到最高帧率

typedef enum
{
    OV7725_FPS_50   = 50  ,
    OV7725_FPS_75   = 75  ,
    OV7725_FPS_100  = 100 ,
    OV7725_FPS_112  = 112 ,
    OV7725_FPS_150  = 150 ,
}ov7725_fps_enum;

typedef enum
{
    OV7725_INIT                 = 0x00,
    OV7725_RESERVE,
    OV7725_CONTRAST,
    OV7725_FPS,
    OV7725_COL,
    OV7725_ROW,
    OV7725_CONFIG_FINISH,

    OV7725_GET_WHO_AM_I         = 0xEF,
    OV7725_GET_STATUS           = 0xF1,
    OV7725_GET_VERSION          = 0xF2,

    OV7725_SET_ADDR             = 0xFE,
    OV7725_SET_DATA             = 0xFF,
}ov7725_cmd_enum;

extern vuint8       ov7725_finish_flag;                                         //一场图像采集完成标志位
extern uint8        ov7725_image_binary[OV7725_H][OV7725_W / 8];

uint16      ov7725_get_version      (void);
uint8       ov7725_init             (void);

#endif
